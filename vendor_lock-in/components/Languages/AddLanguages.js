import React from "react";
import {
  Grid,
  Paper,
  Typography,
  TextField,
  makeStyles,
  IconButton,
  darken,
} from "@material-ui/core";
import { useSnackbar } from "notistack";
import { CloseOutlined } from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles((theme) => ({
  addLanguageButton: {
    borderRadius: 15,
    padding: 16,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    cursor: "pointer",
    background: "#2196f3",
    color: "white",
    "&:hover": {
      background: darken("#2196f3", 0.1),
      color: "white",
    },
  },
}));

export default function AddLanguages({
  languages,
  updateLanguagePairsTable,
  url,
}) {
  const action = (key) => (
    <IconButton
      onClick={() => {
        closeSnackbar(key);
      }}
    >
      <CloseOutlined />
    </IconButton>
  );
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const classes = useStyles();
  const filteredLanguages = languages.filter((e) => e.iso2 !== "");
  const [source, setSource] = React.useState(null);
  const [target, setTarget] = React.useState(null);
  const [benchmark, setBenchmark] = React.useState(null);
  const [transBenchmark, setTransBenchmark] = React.useState(null);
  const [error, setError] = React.useState({
    source: { error: false, helperText: null },
    target: { error: false, helperText: null },
    benchmark: { error: false, helperText: null },
    transBenchmark: { error: false, helperText: null },
  });

  const onAutoCompleteSourceChange = (event, newValue) => {
    event.preventDefault();
    setSource(newValue);
    if (newValue) {
      setError({ ...error, source: { error: false, helperText: null } });
    } else {
      const newError = {
        ...error,
        source: { error: true, helperText: "Source language cannot be null" },
      };
      setError(newError);
    }
  };
  const onAutoCompleteTargetChange = (event, newValue) => {
    event.preventDefault();
    setTarget(newValue);
    if (newValue) {
      setError({ ...error, target: { error: false, helperText: null } });
    } else {
      const newError = {
        ...error,
        target: { error: true, helperText: "Target language cannot be null" },
      };
      setError(newError);
    }
  };

  const handleBenchmarkChage = (event) => {
    event.preventDefault();
    setBenchmark(Number(event.target.value));
    if (
      Math.sign(Number(event.target.value)) !== -1 &&
      Number(event.target.value) !== 0
    ) {
      setError({ ...error, benchmark: { error: false, helperText: null } });
    }
  };

  const handleTransBenchmarkChage = (event) => {
    event.preventDefault();
    setTransBenchmark(Number(event.target.value));
    if (
      Math.sign(Number(event.target.value)) !== -1 &&
      Number(event.target.value) !== 0
    ) {
      setError({ ...error, transBenchmark: { error: false, helperText: null } });
    }
  };

  const onClickAddLanguagePair = (event) => {
    event.preventDefault();
    if (
      source &&
      target &&
      benchmark &&
      transBenchmark &&
      Math.sign(transBenchmark) !== -1 &&
      transBenchmark !== 0 &&
      Math.sign(benchmark) !== -1 &&
      benchmark !== 0
    ) {
      fetch(`${url}/api/languages`, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
        referrerPolicy: "no-referrer",
        body: JSON.stringify({
          name: source.iso2 + target.iso2,
          source_language: source._id,
          target_language: target._id,
          production_status: "A",
          benchmark,
          translator_benchmark: transBenchmark
        }),
      }).then((resp) => {
        if (resp.status !== 200) {
          if (resp.status === 400) {
            resp.json().then((apiError) => {
              if (apiError.error.code === 11000) {
                enqueueSnackbar(
                  `Error. Langauge pair ${apiError.error.key.name} already exists`,
                  {
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "center",
                    },
                    variant: "error",
                    autoHideDuration: 5000,
                    key: "refresh",
                    action,
                  }
                );
              }
            });
          }
        } else {
          resp.json().then((apiResponse) => {
            if (apiResponse) {
              updateLanguagePairsTable(apiResponse);
              enqueueSnackbar(
                `Langauge pair ${apiResponse.name} added successfully!`,
                {
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "center",
                  },
                  variant: "success",
                  autoHideDuration: 2000,
                  key: "refresh",
                  action,
                }
              );
            }
          });
        }
      });
    } else {
      if (!source) {
        const newError = {
          ...error,
          source: { error: true, helperText: "Source language cannot be null" },
        };
        setError(newError);
      }

      if (!target) {
        const newError = {
          ...error,
          target: { error: true, helperText: "Target language cannot be null" },
        };
        setError(newError);
      }

      if (!benchmark || Math.sign(benchmark) === -1 || benchmark === 0) {
        const newError = {
          ...error,
          benchmark: {
            error: true,
            helperText: "Benchmark value cannot be null or negative value",
          },
        };
        setError(newError);
      }

      if (!transBenchmark || Math.sign(transBenchmark) === -1 || transBenchmark === 0) {
        const newError = {
          ...error,
          transBenchmark: {
            error: true,
            helperText: "Translator Benchmark value cannot be null or negative value",
          },
        };
        setError(newError);
      }

      if (
        !source &&
        !target &&
        (!benchmark || Math.sign(benchmark) === -1 || benchmark === 0) &&
        (!transBenchmark || Math.sign(transBenchmark) === -1 || transBenchmark === 0)
      ) {
        const newError = {
          ...error,
          source: { error: true, helperText: "Source language cannot be null" },
          target: { error: true, helperText: "Target language cannot be null" },
          benchmark: {
            error: true,
            helperText: "Benchmark value cannot be null or negative value",
          },
          transBenchmark: {
            error: true,
            helperText: "Translator Benchmark value cannot be null or negative value",
          },
        };
        setError(newError);
      }
    }
  };

  return (
    <Grid item container spacing={3} justify="center">
      <Grid item xs={12}>
        <Typography style={{ padding: 8, fontSize: "1.1rem" }}>
          Please set <b>source</b>, <b>target</b> and <b>benchmark</b> values to
          add new language pair
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <Paper style={{ borderRadius: 0, padding: 16 }}>
          <Autocomplete
            id={`handle-combo-source`}
            options={filteredLanguages}
            size="small"
            getOptionLabel={(option) => option.iso2}
            style={{ width: "100%" }}
            value={filteredLanguages.iso2}
            onChange={(event, newValue) =>
              onAutoCompleteSourceChange(event, newValue)
            }
            clearOnEscape={true}
            renderInput={(params) => (
              <TextField
                {...params}
                label={"Source ISO2"}
                variant="outlined"
                error={error.source.error}
                helperText={error.source.helperText}
              />
            )}
          />
        </Paper>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <Paper style={{ borderRadius: 0, padding: 16 }}>
          <Autocomplete
            id={`handle-combo-target`}
            options={filteredLanguages}
            size="small"
            getOptionLabel={(option) => option.iso2}
            style={{ width: "100%" }}
            value={filteredLanguages.iso2}
            onChange={(event, newValue) =>
              onAutoCompleteTargetChange(event, newValue)
            }
            clearOnEscape={true}
            renderInput={(params) => (
              <TextField
                {...params}
                label={"Target ISO2"}
                variant="outlined"
                error={error.target.error}
                helperText={error.target.helperText}
              />
            )}
          />
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper style={{ borderRadius: 0, padding: 16 }}>
          <TextField
            size="small"
            id="outlined-number"
            label="Benchmark value (numeric)"
            type="number"
            fullWidth
            variant="outlined"
            onChange={handleBenchmarkChage}
            error={error.benchmark.error}
            helperText={error.benchmark.helperText}
          />
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper style={{ borderRadius: 0, padding: 16 }}>
          <TextField
            size="small"
            id="outlined-number"
            label="Translator Benchmark value (numeric)"
            type="number"
            fullWidth
            variant="outlined"
            onChange={handleTransBenchmarkChage}
            error={error.transBenchmark.error}
            helperText={error.transBenchmark.helperText}
          />
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper
          className={classes.addLanguageButton}
          elevation={1}
          onClick={onClickAddLanguagePair}
        >
          <AddIcon />
          <Typography style={{ fontSize: "1rem" }}>
            ADD LANGUAGE PAIR
          </Typography>
        </Paper>
      </Grid>
    </Grid>
  );
}
