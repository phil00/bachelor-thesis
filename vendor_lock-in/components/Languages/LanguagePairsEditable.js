import React from "react";
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  TextField,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    marginTop: theme.spacing(1),
    width: "100%",
    overflowX: "auto",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 650,
  },
  textField: {
    alignItems: "right",
    marginRight: "auto",
    paddingRight: 0,
    paddingBottom: 0,
    marginTop: 0,
    padding: 0,
  },
  input: {
    color: "white",
  },
}));

export function LanguagePairsEditable({
  selectedRows,
  benchmark,
  transBenchmark,
  type,
  handleBenchmarkChange,
  handleTransBenchmarkChange
}) {
  const classes = useStyles();

  const tableRow =
    type === "edit" ? [selectedRows[selectedRows.length - 1]] : selectedRows;

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Table className={classes.table} size="small">
          <TableHead>
            <TableRow>
              <TableCell align="left">Name</TableCell>
              <TableCell align="left">Source</TableCell>
              <TableCell align="left">Target</TableCell>
              <TableCell align="right">
                {type === "edit" ? "Benchmark (editable)" : "Benchmark"}
              </TableCell>
              <TableCell align="right">
                {type === "edit" ? "Trans. Benchmark (editable)" : "Trans. Benchmark"}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tableRow
              ? tableRow.map((row) => {
                  return (
                    <TableRow key={row.name}>
                      <TableCell align="left">{row.name}</TableCell>
                      <TableCell align="left">
                        {row.source_language.iso2}
                      </TableCell>
                      <TableCell align="left">
                        {row.target_language.iso2}
                      </TableCell>
                      <TableCell align="right">
                        {type === "edit" ? (
                          <TextField
                            type="number"
                            id={`outlined-input-${row.name}`}
                            value={benchmark ? benchmark : row.benchmark}
                            onChange={(e) =>
                              handleBenchmarkChange(e, row.name, row)
                            }
                            variant="outlined"
                            inputProps={{
                              style: {
                                textAlign: "right",
                                padding: 8,
                              },
                            }}
                            InputProps={{ className: classes.textField }}
                          />
                        ) : (
                          row.benchmark
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {type === "edit" ? (
                          <TextField
                            type="number"
                            id={`outlined-input-${row.name}`}
                            value={transBenchmark ? transBenchmark : row.translator_benchmark}
                            onChange={(e) =>
                              handleTransBenchmarkChange(e, row.name, row)
                            }
                            variant="outlined"
                            inputProps={{
                              style: {
                                textAlign: "right",
                                padding: 8,
                              },
                            }}
                            InputProps={{ className: classes.textField }}
                          />
                        ) : (
                          row.translator_benchmark
                        )}
                      </TableCell>
                    </TableRow>
                  );
                })
              : ""}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}

export const Recipients = React.memo(LanguagePairsEditable);
