import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Slide,
  IconButton,
  Tooltip,
  DialogContentText,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { LanguagePairsEditable } from "./LanguagePairsEditable";
import { useSnackbar } from "notistack";
import { CloseOutlined } from "@material-ui/icons";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const EditLanguages = ({
  rows,
  selected,
  editLanguagePairs,
  updateSelected,
  type,
  url,
}) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const action = (key) => (
    <IconButton
      onClick={() => {
        closeSnackbar(key);
      }}
    >
      <CloseOutlined />
    </IconButton>
  );
  const [positive, setPositive] = React.useState(false);
  const [benchmark, setBenchmark] = React.useState(false);
  const [transBenchmark, setTransBenchmark] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [editedRow, setEditedRow] = React.useState(false);
  const selectedRows = rows.filter((e) => selected.includes(e.name));

  const handleBenchmarkChange = (event, name, row) => {
    row.benchmark = Number(event.target.value);
    setEditedRow(row);
    setBenchmark(Number(event.target.value));
  };

  const handleTransBenchmarkChange = (event, name, row) => {
    row.translator_benchmark = Number(event.target.value);
    setEditedRow(row);
    setTransBenchmark(Number(event.target.value));
  };

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
    setPositive(false);
  }
  function handleClosePositive() {
    setPositive(true);
  }

  function handleSend() {
    setOpen(false);
    window.alert("you clicked OK!");
  }
  function handleEdit() {
    setOpen(false);
    let stateCopy = [...rows];
    const index = stateCopy.findIndex((e) => e.name === editedRow.name);
    stateCopy.splice(index, 1, editedRow);
    console.log(editedRow)
    fetch(`${url}/api/languages`, {
      method: "PATCH",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: "no-referrer",
      body: JSON.stringify(editedRow),
    })
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        enqueueSnackbar(`Langauge pair ${data.name} updated successfully!`, {
          anchorOrigin: {
            vertical: "top",
            horizontal: "center",
          },
          variant: "success",
          autoHideDuration: 2000,
          key: "refresh",
          action,
        });
        updateSelected();
        editLanguagePairs(stateCopy);
      });
  }
  function handleDelete() {
    setOpen(false);
    let stateCopy = [...rows];
    stateCopy = stateCopy.filter((e) => !selected.includes(e.name));

    fetch(`${url}/api/languages`, {
      method: "DELETE",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: "no-referrer",
      body: JSON.stringify({ deleteArray: selected }),
    })      
      .then(() => {
        enqueueSnackbar(`Langauge pairs deleted!`, {
          anchorOrigin: {
            vertical: "top",
            horizontal: "center",
          },
          variant: "success",
          autoHideDuration: 2000,
          key: "refresh",
          action,
        });
        updateSelected();
        editLanguagePairs(stateCopy);
      });
  }

  return (
    <div>
      <Tooltip title={type === "delete" ? "Delete" : "Edit"}>
        <IconButton
          aria-label={type === "delete" ? "delete" : "edit"}
          onClick={handleClickOpen}
          style={{ color: "#24292E" }}
        >
          {type === "delete" ? <DeleteIcon /> : <EditIcon />}
        </IconButton>
      </Tooltip>
      <Dialog
        maxWidth={"lg"}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby={
          type === "delete"
            ? "alert-dialog-slide-delete-languagePairs"
            : "alert-dialog-slide-edit-languagePairs"
        }
        aria-describedby={
          type === "delete"
            ? "alert-dialog-slide-description-delete"
            : "alert-dialog-slide-description-edit"
        }
      >
        <DialogTitle
          id={
            type === "delete"
              ? "alert-dialog-slide-title-delete"
              : "alert-dialog-slide-title-edit"
          }
        >
          {type === "delete"
            ? "Delete language pairs"
            : "Edit selected language pairs"}
        </DialogTitle>
        <DialogContent>
          {positive && type !== "edit" ? (
            <DialogContentText>
              Are you sure you want to delete selected language pairs?
            </DialogContentText>
          ) : positive && type !== "delete" ? (
            <DialogContentText>
              Are you sure you want to update selected language pair?
            </DialogContentText>
          ) : (
            <LanguagePairsEditable
              selectedRows={selectedRows}
              type={type}
              handleBenchmarkChange={handleBenchmarkChange}
              handleTransBenchmarkChange={handleTransBenchmarkChange}
              benchmark={benchmark}
              transBenchmark={transBenchmark}
            />
          )}
        </DialogContent>
        <DialogActions>
          <div>
            {<Button onClick={handleClose}> CANCEL </Button>}
            {positive && type !== "edit" ? (
              <Button onClick={handleDelete} style={{ color: "red" }}>
                YES
              </Button>
            ) : positive && type !== "delete" ? (
              <Button onClick={handleEdit} style={{ color: "red" }}>
                YES
              </Button>
            ) : (
              <Button onClick={handleClosePositive}> OK </Button>
            )}
          </div>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditLanguages;
