import React from "react";
import Link from "next/link";
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
} from "@material-ui/core";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import HomeIcon from "@material-ui/icons/Home";
import WatchIcon from "@material-ui/icons/Watch";
import AssignmentIcon from "@material-ui/icons/Assignment";
import TranslateIcon from "@material-ui/icons/Translate";
import AccountBalanceWalletIcon from "@material-ui/icons/AccountBalanceWallet";

import pagesJSON from "./pagesJSON.json";

import { useRouter } from "next/router";

let icons = {
  PeopleAltIcon: <PeopleAltIcon />,
  WatchIcon: <WatchIcon />,
  AssignmentIcon: <AssignmentIcon />,
  TranslateIcon: <TranslateIcon />,
  AccountBalanceWalletIcon: <AccountBalanceWalletIcon />,
};

export default function DrawerList(props) {
  let route = useRouter();
  route = route.route;
  return (
    <List>
      <Link href="/">
        <a>
          <ListItem
            button
            onClick={() => props.setSwitching(true)}
            key={"home"}
          >
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={"Home"} />
          </ListItem>
        </a>
      </Link>
      <Divider />
      {pagesJSON.map((element) => {
        {
          return (
            <Link href={element.href} key={element.name}>
              <a>
                <ListItem
                  button
                  onClick={() => props.setSwitching(true)}
                  key={element.name}
                >
                  <ListItemIcon
                    style={{
                      color:
                        route === "/app/queue" && element.icon === "WatchIcon"
                          ? "#009688"
                          : route === "/app/collaborators" &&
                            element.icon === "PeopleAltIcon"
                          ? "#2196f3"
                          : route === "/app/tasks" &&
                            element.icon === "AssignmentIcon"
                          ? "#d500f9"
                          : route === "/app/languages" &&
                            element.icon === "TranslateIcon"
                          ? "#FFB300"
                          : route === "/app/payroll" &&
                            element.icon === "AccountBalanceWalletIcon"
                          ? "#00e676"
                          : "",
                    }}
                  >
                    {icons[element.icon]}
                  </ListItemIcon>
                  <ListItemText
                    style={{
                      textTransform: "capitalize",
                      color:
                        route === "/app/queue" && element.icon === "WatchIcon"
                          ? "#009688"
                          : route === "/app/collaborators" &&
                            element.icon === "PeopleAltIcon"
                          ? "#2196f3"
                          : route === "/app/tasks" &&
                            element.icon === "AssignmentIcon"
                          ? "#d500f9"
                          : route === "/languages" &&
                            element.icon === "TranslateIcon"
                          ? "#FFB300"
                          : route === "/app/payroll" &&
                            element.icon === "AccountBalanceWalletIcon"
                          ? "#00e676"
                          : "",
                    }}
                    primary={element.name}
                  />
                </ListItem>
              </a>
            </Link>
          );
        }
      })}
    </List>
  );
}
