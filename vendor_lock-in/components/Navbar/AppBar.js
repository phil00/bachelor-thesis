import React, { Fragment } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Brightness3Icon from "@material-ui/icons/Brightness3";
import Brightness7Icon from "@material-ui/icons/Brightness7";
import { useTheme } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";
import InfoIcon from "@material-ui/icons/Info";
import Switch from "@material-ui/core/Switch";
import clsx from "clsx";
import DrawerList from "./DrawerList";
import versionJSON from "../../version.json";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    boxShadow: "none",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
}));

export default function ButtonAppBar(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  // const move = (href) => {
  //   router.push(`/app/${href}`);
  // };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
      tabIndex="0"
    >
      <DrawerList
        setSwitching={props.setSwitching}
        globalUser={props.globalUser}
      />
    </div>
  );
  return (
    <div className={classes.root}>
      <AppBar
        position="static"
        style={{ backgroundColor: theme.palette.primary }}
      >
        <Toolbar>
          {
            <>
              <IconButton
                edge="start"
                color="inherit"
                aria-label="theme color"
                title="Theme changer"
                onClick={toggleDrawer("left", true)}
              >
                <MenuIcon />
              </IconButton>
              <Drawer
                anchor={"left"}
                open={state["left"]}
                onClose={toggleDrawer("left", false)}
              >
                {list("left")}
              </Drawer>
              <div
                style={{ display: "flex" }}
                title={`Atlantis tasks version ${versionJSON.version} - ${versionJSON.text}`}
              >
                <InfoIcon fontSize="small" />
                <Typography style={{ marginLeft: 3 }} variant="subtitle2">
                  {`AT ${versionJSON.version}`}
                </Typography>
              </div>
            </>
          }

          <Typography className={classes.title}></Typography>
          <IconButton
            style={{ color: "white" }}
            title={`Turn on ${props.dark ? "light" : "dark"} mode`}
            onClick={() => {
              props.setDark((value) => {
                try {
                  localStorage.setItem("darkThemeAT", !value);
                } catch (error) {
                  console.log(error);
                }
                return !value;
              });
            }}
          >
            {props.dark ? <Brightness7Icon /> : <Brightness3Icon />}
          </IconButton>
          <>
            <Typography style={{ width: 40, fontWeight: "bold" }}>
              {"BP"}
            </Typography>
          </>
        </Toolbar>
      </AppBar>
    </div>
  );
}
