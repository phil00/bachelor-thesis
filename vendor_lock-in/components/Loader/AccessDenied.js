import React from "react";
import CancelIcon from "@material-ui/icons/Cancel";
import { Button, Typography } from "@material-ui/core";
import Link from "next/link";

export default function AccessDenied(props) {
  return (
    <div
      style={{
        flex: 1,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        padding: "0.5em",
      }}
    >
      <CancelIcon
        style={{
          fontSize: "250px",
          color: "#DB5461",
        }}
      />
      <Typography variant="h3" component="h1" align="center">
        {props.text}
      </Typography>
      <Typography variant="subtitle1" align="center">
        {props.info}
      </Typography>
      <Link href={props.link ? props.link : "/"}>
        <Button variant="contained" color="primary">
          {props.link ? "Leave this page!" : "Go to Login!"}
        </Button>
      </Link>
      {props.link ? null : <div className="spinner"></div>}
    </div>
  );
}
