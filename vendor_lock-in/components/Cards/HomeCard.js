import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Link from "next/link";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 300,
    width: "25%",
    height: 150,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    margin: theme.spacing(2),
    transition: "transform 0.5s, border 0.5s",
    borderBottom: "5px solid " + theme.palette.background.paper,
    "&:hover": {
      transform: "translate(0px,-10px)",
      borderColor: theme.palette.primary.main,
    },
  },
  onHoverQueue: {
    "&:hover": {
      borderColor: "#009688",
    },
  },
  onHoverCollabs: {
    "&:hover": {
      borderColor: "#2196f3",
    },
  },
  onHoverTasks: {
    "&:hover": {
      borderColor: "#d500f9",
    },
  },
  onHoverLanguages: {
    "&:hover": {
      borderColor: theme.palette.heroOrange.main,
    },
  },
  onHoverPayroll: {
    "&:hover": {
      borderColor: "#00e676",
    },
  },
  buttonRoot: {
    backgroundColor: theme.palette.primary.main,
    color: "white",
    marginBottom: 16,
  },
  buttonQueue: {
    "&:hover": {
      background: "#009688",
      color: "white",
    },
  },
  buttonCollabs: {
    "&:hover": {
      background: "#2196f3",
      color: "white",
    },
  },
  buttonTasks: {
    "&:hover": {
      background: "#d500f9",
      color: "white",
    },
  },
  buttonLanguages: {
    "&:hover": {
      background: theme.palette.heroOrange.main,
      color: "white",
    },
  },
  buttonPayroll: {
    "&:hover": {
      background: "#00e676",
      color: "white",
    },
  },
  content: { height: 100 },
  actions: { heihgt: 50, justifySelf: "flex-end" },
}));

export default function SimpleCard(props) {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Card
      className={`${classes.root} ${
        props.cardData.name === "queue"
          ? classes.onHoverQueue
          : props.cardData.name === "collaborators"
          ? classes.onHoverCollabs
          : props.cardData.name === "tasks"
          ? classes.onHoverTasks
          : props.cardData.name === "languages"
          ? classes.onHoverLanguages
          : props.cardData.name === "payroll"
          ? classes.onHoverPayroll
          : ""
      }`}
    >
      <CardContent className={classes.content}>
        <Typography
          variant="h5"
          component="h2"
          style={{ textTransform: "uppercase", marginBottom: ".3em" }}
        >
          {props.cardData.name}
        </Typography>
        <Typography
          variant="body2"
          component="p"
          style={{ textOverflow: "ellipsis" }}
        >
          {props.cardData.description}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions}>
        <Link href={`${props.cardData.href}`}>
          <Button
            className={`${classes.buttonRoot} ${
              props.cardData.name === "queue"
                ? classes.buttonQueue
                : props.cardData.name === "collaborators"
                ? classes.buttonCollabs
                : props.cardData.name === "tasks"
                ? classes.buttonTasks
                : props.cardData.name === "languages"
                ? classes.buttonLanguages
                : props.cardData.name === "payroll"
                ? classes.buttonPayroll
                : ""
            }`}
            onClick={() => props.setSwitching(true)} 
            fullWidth
          >{`Go to ${props.cardData.name}`}</Button>
        </Link>
      </CardActions>
    </Card>
  );
}
