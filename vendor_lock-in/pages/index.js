import React, { useEffect } from "react";
import { Container, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import Head from "next/head";
import { useTheme } from "@material-ui/core/styles";
import Card from "../components/Cards/HomeCard";

import pagesJSON from "../components/Navbar/pagesJSON.json";

export default function main(props) {  
  const theme = useTheme();
  const router = useRouter();
  return (
    <>
      <Head>
        <title>{`Home Page`}</title>
      </Head>
      <Container
        maxWidth="lg"
        style={{
          padding: theme.spacing(2),
        }}
      >        
        <div
          key="cards"
          style={{
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            alignContent: "center",
            justifyContent: "center",
          }}
        >
          {pagesJSON.map((element) => {
            if (
              element.everyone === true ||
              allowedUsers.includes(props.globalUser)
            ) {
              return (
                <Card
                  key={element.name}
                  cardData={element}
                  setSwitching={props.setSwitching}
                />
              );
            }
          })}
        </div>
      </Container>
    </>
  );
}
