import nextConnect from "next-connect";
import mongoose from "mongoose";
const Languages = require("../../models/language");
const LanguagePairs = require("../../models/language_pair");

const handler = nextConnect();

const mongoURI = "mongodb+srv://admin:fGae7Nv2WRIAo8oZ@cluster0.k4l7r.mongodb.net/test?authSource=admin&replicaSet=atlas-rmkok8-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true"

async function checkMongoose(res) {
  if (mongoose.connection.readyState === 0) {
    return await mongoose.connect(mongoURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      dbName: "bachelor-thesis",
    });
  }
}

async function closeConnection() {
  if (
    mongoose.connection.readyState !== 0 &&
    mongoose.connection.readyState !== 3
  ) {
    await mongoose.connection.close();
  }
}

handler.get(async (req, res) => {  
  try {
    let check = await checkMongoose(res);
    if (check !== false) {
      let [languages, language_pairs] = await Promise.all([
        Languages.find({}).lean(),
        LanguagePairs.find({})
          .populate("source_language")
          .populate("target_language")
          .lean(),        
      ]);      
      closeConnection();
      res.json({ language_pairs, languages });
    }
  } catch (error) {
    res.json({error})
  }
});

handler.post(async (req, res) => {
  try {
    let check = await checkMongoose(res);
    if (check !== false) {
      const languagePair = new LanguagePairs(req.body);
      let languagePairSaved = await languagePair.save();
      languagePairSaved = await languagePairSaved
        .populate("source_language")
        .populate("target_language")
        .execPopulate();
      closeConnection();
      res.json(languagePairSaved);
    }
  } catch (error) {
    if (error.name === "MongoError") {
      console.log(error.code === 11000);

      console.log({
        MongoError: {
          name: error.name,
          code: error.code,
          key: error.keyValue ? error.keyValue : "",
        },
      });
      if (error.code === 11000) {
        res.status(400).json({
          error: { name: error.name, code: error.code, key: error.keyValue },
        });
      } else {
        res.status(500);
      }
    } else {
      console.log(error);
    }
  }
});

handler.patch(async (req, res) => {
  try {
    let check = await checkMongoose(res);
    if (check !== false) {
      let response = await LanguagePairs.findOneAndUpdate(
        { name: req.body.name },
        {
          benchmark: req.body.benchmark,
          translator_benchmark: req.body.translator_benchmark,
        },
        // { upsert: true }
        { new: true }
      )
        .populate("source_language")
        .populate("target_language");
      closeConnection();
      res.json(response._doc);
    }
  } catch (error) {
    if (error.name === "MongoError") {
      console.log(error.code === 11000);
      closeConnection();
      console.log({
        MongoError: {
          name: error.name,
          code: error.code,
          key: error.keyValue ? error.keyValue : "",
        },
      });
      if (error.code === 11000) {
        res.status(400).json({
          error: { name: error.name, code: error.code, key: error.keyValue },
        });
      } else {
        res.status(500);
      }
    } else {
      console.log(error);
    }
  }
});

handler.delete(async (req, res) => {
  try {
    let check = await checkMongoose(res);
    if (check !== false) {
      let response = await LanguagePairs.deleteMany({
        name: { $in: req.body.deleteArray },
      });
      closeConnection();
      res.json({ response });
    }
  } catch (error) {
    console.log(error)
  }
});

export default handler;
