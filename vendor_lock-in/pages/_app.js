import "../styles/globals.css";
import Head from "next/head";
import React, { useState } from "react";
import Amplify from "@aws-amplify/core";
import Auth from "@aws-amplify/auth";
import { SnackbarProvider } from "notistack";
import PropTypes from "prop-types";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { darken } from "@material-ui/core";
import AppBar from "../components/Navbar/AppBar";
import { useRouter } from "next/router";

import Loader from "../components/Loader/Loader";

//require("dotenv").config();

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [dark, setDark] = useState(false);
  const [logged, setLogged] = useState(false);
  const [switching, setSwitching] = useState(false);
  const [globalUser, setGlobalUser] = useState(
    process.env.NODE_ENV === "development" ? "PHL" : null
  );
  const darkTheme = createMuiTheme({
    palette: {
      type: "dark",
      primary: { main: "#2196f3" },
      secondary: { main: "#FFB300" },
      heroOrange: {
        main: "#FFB300",
      },
      heroBlue: {
        main: "#2196f3",
      },      
      actionButton: {
        main: "#2196f3",
      },
      actionText: {
        main: "#fff",
      },
    },
  });
  const lightTheme = createMuiTheme({
    palette: {
      type: "light",
      background: { default: "#F6F8FA" },
      primary: { main: "#24292e" },
      secondary: { main: "#2196f3" },

      heroOrange: {
        main: "#FFB300",
      },
      heroBlue: {
        main: "#2196f3",
      },      
      actionButton: {
        main: "#F18F20",
      },
      actionText: {
        main: "#fff",
      },
      background: {
        default: "#ebebeb",
      },
    },
  });
  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  React.useEffect(() => {
    setDark(() => {
      let darkTheme = localStorage.getItem("darkThemeAT");
      if (darkTheme !== null) {
        return darkTheme === "true";
      } else {
        if (
          window.matchMedia &&
          window.matchMedia("(prefers-color-scheme: dark)").matches
        ) {
          localStorage.setItem("darkThemeAT", true);
          return true;
        } else {
          localStorage.setItem("darkThemeAT", false);
          return false;
        }
      }
    });
  }, []);

  React.useEffect(() => {
    setSwitching(false);
  }, [router]);

  return (
    <>
      <ThemeProvider theme={dark ? darkTheme : lightTheme}>
        <Head>          
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
        </Head>

        <CssBaseline />
        <SnackbarProvider maxSnack={2}>
          <AppBar
            //style={{ boxShadow: "none" }}
            logged={logged}
            dark={dark}
            setDark={setDark}
            globalUser={globalUser}
            setSwitching={setSwitching}
          />
          {switching ? (
            <Loader />
          ) : (
            <Component
              setSwitching={setSwitching}
              globalUser={globalUser}
              setGlobalUser={setGlobalUser}
              logged={logged}
              setLogged={setLogged}
              {...pageProps}
            />
          )}
        </SnackbarProvider>
      </ThemeProvider>
    </>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};

export default MyApp;
