import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Grid, useTheme, makeStyles } from "@material-ui/core";
import Head from "next/head";
import Loader from "../components/Loader/Loader";
import LanguagePairsTable from "../components/Languages/LanguagePairsTable";
import AddLanguages from "../components/Languages/AddLanguages";

const url =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000"
    : ``;

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 24,
    [theme.breakpoints.down("sm")]: {
      padding: 16,
    },
  },
}));

export default function Collaborators() {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const [languages, setLanguages] = useState(null);
  const [fetching, setFetching] = useState(true);
  const [languagePairs, setLanguagePairs] = useState(null);
  const updateLanguagePairsTable = (updateObj) => {
    setLanguagePairs([...languagePairs, updateObj]);
  };
  const editLanguagePairs = (newArray) => {
    setLanguagePairs(newArray);
  };

  useEffect(() => {
    async function fetchData() {
      let query = await fetch(`${url}/api/languages`);
      query = await query.json();
      setLanguages(query.languages);
      setLanguagePairs(query.language_pairs);
      setFetching(false);
    }
    fetchData();
  }, []);

  return (
    <>
      <Head>
        <title>{`Languages`}</title>
      </Head>
      {fetching ? (
        <Loader />
      ) : (
        <>
          <Grid container>
            <Grid item xs={12} sm={12} md={6} className={classes.root}>
              <LanguagePairsTable
                language_pairs={languagePairs}
                length={languagePairs.length}
                editLanguagePairs={editLanguagePairs}
                url={url}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6} className={classes.root}>
              <AddLanguages
                url={url}
                languages={languages}
                updateLanguagePairsTable={updateLanguagePairsTable}
              />
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}
