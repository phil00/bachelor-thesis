import nextConnect from "next-connect";
import database from "../utils/mongooseSetup";

const middleware = nextConnect();

middleware.use(database);

export default middleware;
