import { MongoClient } from "mongodb";
import nextConnect from "next-connect";

const client = new MongoClient(
  "mongodb+srv://lambdaSync:SnJ8mwx9QsS2Zx9Z@cluster0.naxpy.mongodb.net/test?authSource=admin&replicaSet=atlas-4gvawx-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

async function database(req, res, next) {
  if (!client.isConnected()) await client.connect();
  req.dbClient = client;
  req.db = client.db(process.env.DBUSE === "production" ? "atlantis-tasks": "dev-atlantis-tasks");
  return next();
}

const middleware = nextConnect();

middleware.use(database);

export default middleware;
