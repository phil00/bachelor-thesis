const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// https://mongoosejs.com/docs/discriminators.html

const LanguagePairsSchema = new Schema({
  name: { type: String, required: true, unique: true, dropDups: true },
  source_language: {
    type: Schema.Types.ObjectId,
    ref: "Language",
    required: true,
  },
  target_language: {
    type: Schema.Types.ObjectId,
    ref: "Language",
    required: true,
  },
  production_status: { type: String, required: true },
  benchmark: { type: Number, required: true },
  translator_benchmark: { type: Number, required: true },
});

LanguagePairsSchema.pre("save", function (next) {
  this.name = this.name.toUpperCase();
  next();
});

module.exports =
  mongoose.models.Language_pair ||
  mongoose.model("Language_pair", LanguagePairsSchema);
