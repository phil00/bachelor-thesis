const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// https://mongoosejs.com/docs/discriminators.html

const LanguageSchema = new Schema({
  name: { type: String, required: true },
  iso2: { type: String, required: false },
  iso3: { type: String, required: true },
});
module.exports =
  mongoose.models.Language || mongoose.model("Language", LanguageSchema);
