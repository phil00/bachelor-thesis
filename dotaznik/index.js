console.log("Loading function");
const aws = require('aws-sdk');
const s3 = new aws.S3({ apiVersion: '2006-03-01' });
const axios = require("axios");

exports.handler = async (event, context) => {
  const paramsBucket = {
    Bucket: "poller-data",
    Key: "generations.json",
  };
  const generations = await s3.getObject(paramsBucket).promise();
  const generationsData =await JSON.parse(generations.Body)
  const { data } = await axios("https://pokeapi.co/api/v2/generation/");
  if (generationsData.results.length == data.results.length) {
    return { change: false, text: "No new generation has been released!", count: data.results.length };
  } else {
    const s3Params = {
      Bucket: "poller-data",
      Key: "generations.json",
      Body: JSON.stringify(data),
      ContentType: "application/json",      
    };
    try {
      await s3.putObject(s3Params).promise();
      const text =
        generations.results.length < data.results.length
          ? "New generation has been released!"
          : "Generation has been eradicated!";
      return { change: true, text: "New generation has been released!", count: data.results.length };
    } catch (error) {
      console.log(error);
      return { change: false, text: "There has been an error saving the file" };
    }
  }
};
